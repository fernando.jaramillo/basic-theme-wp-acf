<?php 
/**
 * 
 * The template for displaying all single posts *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post *
 * @package BRANCH
 **/
get_header();
?>
<main id="single-post-<?php the_ID(); ?>" <?php post_class('single-post-content'); ?>>
  <section class="section-post py-section">
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-12 col-lg-9">
          <h1>Single post</h1>
          <?php echo the_post_thumbnail('full')?>
          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="entry-content">
              <h1><?php __(the_title()); ?></h1>
              <hr>
              <?php the_content();  ?>
            </div><!-- .entry-content -->
          </article>
        </div>

        <div class="col-12">
          <hr>
          <h1>
          <?php
              the_field('titulos_test');
              $iagen = get_field('imagen_test');
              echo wp_get_attachment_image($iagen );
          ?>
          </h1>
          
        </div>

      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>