<?php
  // Case: Read More
  if( get_row_layout() == 'sub_component_read_more' ):
    get_template_part('template-parts/components/read_more');
  endif;