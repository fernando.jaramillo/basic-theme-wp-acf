<?php 
  $univa_number_link_tool_free = get_field('univa_number_link_tool_free','options');
  if( $univa_number_link_tool_free ): 
      $univa_tool_free_url    = $univa_number_link_tool_free['url'];
      $univa_tool_free_title  = $univa_number_link_tool_free['title'];
      $univa_tool_free_target = $univa_number_link_tool_free['target'] ? $univa_number_link_tool_free['target'] : '_self';
  endif;

  $repeater_contant_us  = get_field('repeater_contant_us','options');
?>

<!-- Footer -->
<footer class="text-center text-lg-start text-muted footer_univa">

  <!-- Section: Links  -->
  <section class="py-4 section-menu-primary">
    <div class="container-footer container-fluid  text-center container-primary ">
      <button class="btn btn-univa btn-univa-up" type="button" aria-label="go up"><i class="fas fa-chevron-up"></i></button>
      <!-- Grid row -->
      <div class="row">
        <!-- Grid column -->
        <div class="col-12 col-xl-2 order-2 order-xl-1  col-logos-footer">
          <!-- Content -->
          <object type="image/svg+xml"
                  alt ="Universidad del Valle de Atemajac"
                  aria-label="<?php wp_title(); ?>"
                  data="<?php echo get_stylesheet_directory_uri().'/dist/img/logo_theme.svg'; ?>" 
                  class="logo"
                  style="width: 100px;">
              Logo theme
          </object>
        </div>

        <!-- Grid column -->
        <div class="col-12 col-xl-10 order-1 order-xl-2 d-flex align-items-center justify-content-center">
          <div class="row w-100">
            <!-- Grid column -->
            <div class="col-12 col-xl-6 col-menu-footer order-1 px-0 px-lg-5">
              <?php wp_nav_menu( array( 'theme_location' => 'menu-theme-footer' ) ); ?>
            </div>

            <!-- Grid column -->
            <div class="col-12 col-xl-4 d-flex order-3 order-xl-2 col-contact-us ">
              <div class="sub-contact-us">
                <!-- Links -->
                <section>
                  <ul class="list-contact">
                    <?php if( $repeater_contant_us ): foreach ( $repeater_contant_us as $key => $contact ) : if( !$contact['is_link'] ):  /* $contact['icon_contant'] */ ?>
                      <li>
                        <?php if( $contact['icon_contant'] ): ?>
                        <img src="<?php echo $contact['icon_contant'] ?>" alt="Contact UCC" class="icon_contact">
                        <?php endif; ?>
                        <p class="m-0 text-addres">
                          <?php echo $contact['text_contact']; ?>
                        </p>
                      </li>
                      <?php else: $link_target = $contact['text_link']['target'] ? $contact['text_link']['target'] : '_self'; ?>
                      <li>
                        <?php if( $contact['icon_contant'] ): ?>
                        <img src="<?php echo $contact['icon_contant'] ?>" alt="Contact UCC" class="icon_contact">
                        <?php endif; ?>
                        <a  href="<?php echo $contact['text_link']['url'] ?>" rel="noopener" role="link" target="<?php echo $link_target ?>" 
                            aria-label="<?php echo $contact['text_link']['title'] ?>">
                        <?php echo $contact['text_link']['title'] ?>
                        </a>
                      </li>
                    <?php endif; endforeach; endif; ?>
                  </ul>
                </section>
                
              </div>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <?php  if( have_rows('univa_social_media', 'options') ): ?>
              <div class="col-12 col-xl-2  d-flex col-shared-social order-2 order-xl-3">          
                <div class="content-shared">
                  <ul class="list-group list-group-horizontal">
                      <?php  // Loop through rows.
                        while( have_rows('univa_social_media', 'options') ) : the_row();
                            // Load sub field value.
                            $univa_icon_media = get_sub_field('univa-icon-media');
                            $univa_url_media  = get_sub_field('univa-url-media');
                            $univa_link = $univa_url_media ? $univa_url_media : '#';
                            ?>
                            <li class="list-group-item">
                              <a  href="<?php echo $univa_link; ?>" rel="noopener" role="link" target="_blank" 
                                  aria-label="Contact Social Media" > <i class="<?php echo $univa_icon_media; ?>"></i> </a>
                            </li>
                        <?php
                        // End loop.
                        endwhile;    ?>   
                  </ul>

                  <?php
                    $univa_foo_button_action = get_field('univa_button_action_footer','options');
                    $univa_foo_link_title = $univa_foo_button_action['title'];
                    $univa_foo_link_url = $univa_foo_button_action['url'];
                    $univa_foo_link_target = $univa_foo_button_action['target'] ? $univa_foo_button_action['target'] : '_self';
                    ?>
                  <?php if( is_array( $univa_foo_button_action) && count($univa_foo_button_action) > 0 ): ?>
                    <button class="btn btn-univa btn-univa-outline mb-2 btn-go"
                            aria-label="<?php echo esc_html( $univa_foo_link_title ); ?>" 
                            data-btn-src="<?php echo esc_url( $univa_foo_link_url ); ?>"
                            data-btn-target="<?php echo esc_attr( $univa_foo_link_target ); ?>"
                            type="button">
                    <?php echo esc_html( $univa_foo_link_title ); ?>
                    </button>
                  <?php endif; ?>

                </div>
              </div>
            <!-- Grid column -->
            <?php endif; ?>

          </div>
        </div>
        

      </div>
    </div>
  </section>
  <!-- Section: Links  -->

  <!-- Copyright OK -->
  
  <div class="container-footer container-fluid text-center py-3 container-copy">
    <div class="row">
      <div class="col-sm-12 col-xl-6 col-menu-terms">
        <?php wp_nav_menu( array( 'theme_location' => 'menu-theme-terms' ) ); ?>
      </div>
      <div class="col-sm-12 col-xl-6 col-copy">
        <span>
          <?php the_field('univa_text_copy','options') ;?>
        </span>
      </div>
    </div>
  </div>
  <!-- Copyright OK -->

  <div class="container-footer container-fluid text-center py-4 container-liability">
    <div class="row">
      <div class="col-sm-12 px-0">
        <p class="m-0 text-liability-release">
          <?php the_field('univa_text_liability_release','options') ;?>
        </p>
      </div>
    </div>
  </div>
</footer>