<?php

  $post_id = get_the_ID();
  $title_card = strlen( get_field('title_primary_card') ) > 0  ? get_field('title_primary_card') : get_the_title();
  $post_permalink = get_permalink( $post_id );
  $image_primary = get_field('image_primary_card') ? get_field('image_primary_card') : get_post_thumbnail_id();
  if( is_array( $image_primary) ) { $image_primary = $image_primary['ID']; }
  $label_button = strlen( get_field('label_button_card') ) > 0 ? get_field('label_button_card') : 'Leer Artículo';
  $expert = get_field('extract_card') ? get_field('extract_card') : get_the_excerpt();
?>

<div class="card h-100 card-single-post">
  <a href="<?php echo $post_permalink; ?>" aria-label="<?php echo $title_card; ?>" >
    <?php echo wp_get_attachment_image( $image_primary ,'card-image-single', false, array( "class" => "card-img-top")); ?>
  </a>
  <div class="card-body">
    <h4 class="card-title"><?php echo $title_card; ?></h4>
    <p class="card-text normal-p3"><?php echo $expert; ?></p>
    <div class="button-action text-center">
      <a  href="<?php echo $post_permalink; ?>" 
          aria-label="<?php echo $title_card; ?>"
          class="btn btn-theme btn-theme-normal btn-theme-small">
          <?php echo $label_button; ?>
      </a>
    </div>
  </div>
</div>
