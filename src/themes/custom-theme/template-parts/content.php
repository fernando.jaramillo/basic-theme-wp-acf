<?php
  /**
   * Template part for displaying posts *
   * @link https://developer.wordpress.org/themes/basics/template-hierarchy/ *
   * @package Branch
   */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
    <h1> <?php the_title() ?></h1>
    <hr>
		<?php the_content();	?>
	</div><!-- .entry-content -->
</article>
