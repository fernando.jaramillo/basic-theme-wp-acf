<?php
  $sub_read_more = get_sub_field('sub_read_more');
  $visible_text =  $sub_read_more['visible_text'];
  $complete_text = $sub_read_more['hiden_text'];

?>

<section  class="section-read-more py-5 <?= $sub_read_more['class_custom'] ?>"
          id="<?= $sub_read_more['id_section'] ?>"
          style="background-color: <?= $sub_read_more['background_color'] ?>;">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-12 col-lg-8">
        <h1 class="text-center"><?= $sub_read_more['titulo_section']; ?></h1> <br>
        <div class="read-more-partial ">
          <div class="mb-2 content-read-more">
            <p class="text-read">
              <?php echo  $visible_text; ?>
              <?php if (strlen($complete_text) > 0) : ?>
                <span class="dots" style="display: inline;">...</span>
                <span class="more" style="display: none;">
                  <br><br> <?php echo  $complete_text; ?>
                </span>
              <?php endif; ?>
            </p>
          </div>
          <?php if (strlen($complete_text) > 0) : ?>
            <div class="btn-action-read-more">
              <button class="readBtn" type="button">
                Leer Más <i class="fas fas-down"></i>
              </button>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>