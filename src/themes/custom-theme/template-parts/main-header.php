<nav  class="navbar navbar-expand-lg navbar-custom-theme navbar-light" 
        id="navbar-custom-theme-primary"
        role="navigation" 
        aria-current="true"> 
</nav><?php
  $menu_primary   = wp_get_menu_array( 'menu-theme-primary' );

  $link_outline = get_field( 'theme_button_action_outline' , 'options' );
  if( $link_outline ): 
      $link_url_outline       = $link_outline['url'];
      $link_title_outline     = $link_outline['title']  ? $link_outline['title']  : 'APPLY NOW';
      $link_target_outline    = $link_outline['target'] ? $link_outline['target'] : '_self';
  endif; 

  $link_secondary = get_field( 'theme_button_action_secondary' , 'options' );
  if( $link_secondary ): 
      $link_url_secondary      = $link_secondary['url'];
      $link_title_secondary    = $link_secondary['title']  ? $link_secondary['title']  : 'REQUEST INFO';
      $link_target_secondary   = $link_secondary['target'] ? $link_secondary['target'] : '_self';
  endif; 
?>

  <nav  class="navbar navbar-expand-lg navbar-univa navbar-light" 
        id="navbar-univa-primary"
        role="navigation" 
        aria-current="true"> 
    <div class="container-navbar container-fluid px-0 px-lg-2">
      
      <div class="col col-nabvar-brand d-flex justify-content-end px-5 px-lg-0 position-relative">
      
        <a href="/" aria-label="home ACG" class="target_logo">
            <object type="image/svg+xml" style="pointer-events: none;"
                    alt ="Universidad del Valle de Atemajac"
                    aria-label="<?php wp_title(); ?>"
                    data="<?php echo get_stylesheet_directory_uri().'/dist/img/logo_theme.svg'; ?>" 
                    class="logo">
                Logo theme
            </object>
        </a>

        <button   class="navbar-toggler navbar-mobil-univa" 
                  type="button" data-bs-toggle="collapse" 
                  data-bs-target="#main_nav" 
                  aria-controls="navbarSupportedContent" 
                  aria-expanded="false" 
                  aria-label="Toggle navigation">
            <div class="McButton"> <b></b> <b></b> <b></b>  <span class="text-close">Close</span></div>
        </button>

      </div>
    
      <div  class="collapse navbar-collapse container row m-0 px-4 px-sm-5 px-lg-0 py-3 py-lg-0 justify-content-end" 
            id="main_nav">

        <div class="order-1 order-lg-2 order-xxl-1 col-sm-12 col-lg-12 col-xl-auto py-0 py-lg-0 col-navbar-nav ">
          <ul class="navbar-nav navbar-nav-primary row px-lg-4 m-0">
            <?php foreach ($menu_primary as $key => $nav_item_primary) : ?>
              <?php 
              // dropdown-menu-primary level 2
              if( count( $nav_item_primary['children'] ) > 0 ) :?>
                <li class="col p-0 px-lg-2 nav-item nav-item-primary dropdown 
                            <?php echo implode(" ", $nav_item_primary['classes'] ); ?>"
                    style="min-width: fit-content;">
                  <a  class="nav-link desktop-body-2 wits_blue_text dropdown-toggle" 
                      href="#" data-bs-toggle="dropdown">  
                      <?php  echo $nav_item_primary['title']; ?>
                  </a>
                  <?php $submenu = $nav_item_primary['children']; ?>
                    <ul class="dropdown-menu dropdown-menu-primary">
                      <?php 
                        foreach ($submenu as $key => $dropdown_item): 
                          if( count( $dropdown_item['children'] ) <= 0 ) : ?>
                            <li>
                              <a  class="dropdown-item desktop-body-2 wits_blue_text 
                                        <?php echo implode(" ", $dropdown_item['classes']); ?>" 
                                  href="<?php  echo $dropdown_item['url'];  ?>"> 
                                <?php  echo $dropdown_item['title'];  ?>
                              </a>
                            </li>
                          <?php else : ?>
                            <li>
                                <a  class="dropdown-item is-menu-item <?php echo implode(" ", $dropdown_item['classes']); ?>" href="#"> 
                                    <?php  echo $dropdown_item['title'];  ?> </a> 
                                <?php echo pupulate_submenu_html( $dropdown_item['children'] ); ?>
                            </li>
                          <?php endif;?>
                      <?php endforeach; ?>
                    </ul>
                </li>
              <?php 
                // menu Item level 1 
                else :?>
                <li class="col p-0 nav-item nav-item-primary"  style="min-width: fit-content;">
                  <a  class="nav-link desktop-body-2 wits_blue_text <?php echo implode(" ", $nav_item_primary['classes']); ?>"
                      href="<?php  echo $nav_item_primary['url']; ?> "> 
                    <?php  echo $nav_item_primary['title'];  ?> 
                  </a> 
                </li>
              <?php endif;?>

            <?php endforeach; ?>
          </ul>
        </div>
        
        <div class="order-2 order-lg-1 order-xxl-2 col-sm-12 col-lg-8 col-xl-auto me-lg-5 me-xl-0 py-lg-2 py-xxl-0 col-btn-action d-none d-lg-flex">

          <a  class="btn btn-theme btn-theme-primary btn-theme-small btn-theme-outline button-action-one me-2" 
              style="min-width: 180px;"
              href="<?php echo esc_url( $link_url_outline ); ?>"
              target="<?php echo esc_attr( $link_target_outline ); ?>"
              aria-label="<?php echo esc_html( $link_title_outline ); ?>" 
              type="button" >
              <?php echo esc_html( $link_title_outline ); ?>
          </a>

          <a  class="btn btn-theme btn-theme-primary btn-theme-small button-action-two" 
              style="min-width: 180px;"
              href="<?php echo esc_url( $link_url_secondary ); ?>"
              target="<?php echo esc_attr( $link_target_secondary ); ?>"
              aria-label="<?php echo esc_html( $link_title_secondary ); ?>" 
              type="button" >
                  <?php echo esc_html( $link_title_secondary ); ?>
          </a>

        </div>

      </div>

    </div>
  </nav>


  <div class="col-btn-action-fixed d-block d-lg-none">
    <a  class="btn btn-theme btn-theme-primary btn-theme-small btn-theme-outline button-action-one px-0" 
        href="<?php echo esc_url( $link_url_outline ); ?>"
        target="<?php echo esc_attr( $link_target_outline ); ?>"
        aria-label="<?php echo esc_html( $link_title_outline ); ?>" 
        rel="noopener"
        type="button" >
        <?php echo esc_html( $link_title_outline ); ?>
    </a>

    <a  class="btn btn-theme btn-theme-primary btn-theme-small button-action-two px-0" 
        hrefc="<?php echo esc_url( $link_url_secondary ); ?>"
        target="<?php echo esc_attr( $link_target_secondary ); ?>"
        aria-label="<?php echo esc_html( $link_title_secondary ); ?>" 
        rel="noopener"
        type="button" >
        <?php echo esc_html( $link_title_secondary ); ?>
    </a>
  </div>
  
  
  <!-- Toast Errror Mensage-->
  
  <div class="toast toast-univa" id="toastNotice" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" class="rounded me-2" 
          alt="<?php echo get_bloginfo('name'); ?>" width="22px">
      <strong class="me-auto"><?php wp_title('|', true, 'right'); ?></strong>        
      <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    <div class="toast-body">
      <?php the_field( 'message_error_form' ,'options' ); ?>
    </div> 
  </div>