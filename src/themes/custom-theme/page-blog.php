<?php
/**
 * The Blog Page
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/ *
 * @package Branch
 */
  get_header();

  $args = array(
      'post_type'       => 'post',
      'post_status'     => 'publish',
      'posts_per_page'  => 3,
      'paged'           => 1,
      'orderby' 		    => 'date', 
      'order' 		      => 'ASC'
  );
  $blog_posts = new WP_Query( $args );
?>

  <main id="home-page" class="site-main home-page">
    <section class="section-card-post-list">
      <div class="container py-section">
          <?php if ( $blog_posts->have_posts() ) : ?>
            <div class="row d-flex justify-content-center blog-posts"> 
              <?php while ( $blog_posts->have_posts() ) : $blog_posts->the_post(); 
                  $type = get_post_type( get_the_ID() ); ?>
                  <div class="col-12 col-md-6 col-lg-4 my-2">
                    <?php get_template_part( 'template-parts/partials/card-single-post', 'content' ); ?>
                  </div>
                <?php endwhile; 
              ?>
            </div>

            <div class="row mt-3">
              <div class="col-12 text-center">
                <a  rel="noopener noreferrer"
                    class="btn btn-theme btn-theme-primary btn-theme-small loadmore">Cargar más
                </a>
              </div>
            </div>
          <?php  endif;  ?>
      </div>
    </section>
  </main>

<?php get_footer(); 