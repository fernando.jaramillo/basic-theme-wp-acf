<?php
/**
 * custom-theme functions and definitions *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/ *
 * @package custom-theme
*/

  if ( ! defined( '_S_VERSION' ) ) {
    // Replace the version number of the theme on each release.
    define( '_S_VERSION', '1.0.0' );
  }

  if ( ! function_exists( 'custom_theme_setup' ) ) :	
    function custom_theme_setup() {     
      load_theme_textdomain( 'custom-theme', get_template_directory() . '/languages' );
      add_theme_support( 'automatic-feed-links' );
      add_theme_support( 'title-tag' );
      add_theme_support( 'post-thumbnails' );
      add_theme_support( 'customize-selective-refresh-widgets' );
      add_theme_support(
        'html5',
        array(
          'search-form',
          'comment-form',
          'comment-list',
          'gallery',
          'caption',
          'style',
          'script',
          'post-formats',
          'post-thumbnails',
          'responsive-embeds'
        )
      );
      add_theme_support(
        'custom-logo',
        array(
          'height'      => 250,
          'width'       => 250,
          'flex-width'  => true,
          'flex-height' => true,
        )
      );
      // This theme uses wp_nav_menu() in one location.
      $locations = ( array(
        'menu-theme-primary'  =>   esc_html__( 'Primary Menu', 'custom-theme' ),
        'menu-theme-terms'  =>   esc_html__( 'Terms Menu', 'custom-theme' ),
        'menu-theme-footer'  =>   esc_html__( 'Footer Menu', 'custom-theme' )
      ) );
      register_nav_menus( $locations );
    }
  endif;
  add_action( 'after_setup_theme', 'custom_theme_setup' );

  function custom_theme_thumb_support() {
    add_image_size( 'custom-theme-banner-form-desktop', 1440, 620, false ); // 1440 pixels wide (and unlimited height)
  }
  add_action( 'after_setup_theme', 'custom_theme_thumb_support' );


/**
 * Enqueue scripts and styles.
*/
function custom_theme_scripts() {
  wp_enqueue_style( 'bootstrap_css', get_stylesheet_directory_uri() . '/dist/css/bootstrap.min.css', array(), '5.0.2');
  wp_enqueue_style( 'custom-theme_global_css', get_stylesheet_directory_uri() . '/dist/css/style.css', array(), '1');
  wp_enqueue_style( 'splide_css', get_stylesheet_directory_uri() . '/dist/css/splide.min.css',array(), '1');

  wp_enqueue_script('jquery');
  wp_enqueue_script( 'bootstrap_bundle_js', get_stylesheet_directory_uri() . '/dist/js/bootstrap.bundle.min.js' );
  wp_enqueue_script( 'splide_js', get_stylesheet_directory_uri() . '/dist/js/splide.min.js', null ,  '4.5.2',true);
  wp_enqueue_script( 'custom-theme_global_js', get_stylesheet_directory_uri() . '/dist/js/global.js' );   
  
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'custom_theme_scripts' );


function blog_scripts() {
  // Register the script
  wp_register_script( 'blog-script', get_stylesheet_directory_uri(). '/dist/js/blog-script.js', array('jquery'), false, true );

  // Localize the script with new data
  $script_data_array = array(
      'ajaxurl' => admin_url( 'admin-ajax.php' ),
      'security' => wp_create_nonce( 'load_more_posts' ),
  );
  wp_localize_script( 'blog-script', 'blog', $script_data_array );

  // Enqueued script with localized data.
  wp_enqueue_script( 'blog-script' );
}
add_action( 'wp_enqueue_scripts', 'blog_scripts' );

add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');


//Font Awesome
function enqueue_load_fa() {
	wp_enqueue_style( 'load-fa', 'https://use.fontawesome.com/releases/v5.13.1/css/all.css' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_load_fa' );


/**
 * Custom template tags for this theme.
*/
require get_template_directory() . '/includes/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
*/
require get_template_directory() . '/includes/template-functions.php';

/**
 * Customizer additions.
*/
require get_template_directory() . '/includes/customizer.php';

/**
 * Get items of Menu
*/
require get_template_directory() . '/includes/get_menu_array.php';

/**
 * Get Breadcrumbs
*/
require get_template_directory() . '/includes/get_breadcrumbs.php';


require get_template_directory() . '/includes/theme_options.php';

/* remove DashIcons is not admin */
add_action( 'wp_print_styles', function() {
  if (!is_admin_bar_showing()) wp_deregister_style( 'dashicons' );
}, 100);

function load_posts_by_ajax_callback() {
  check_ajax_referer('load_more_posts', 'security');
  $args = array(
      'post_type' => 'post',
      'post_status' => 'publish',
      'posts_per_page' => $_POST['posts_per_page'],
      'paged' => $_POST['page'], 
      'orderby' 		    => 'date', 
      'order' 		      => 'ASC'
  );
  $blog_posts = new WP_Query( $args );
  ?>

  <?php if ( $blog_posts->have_posts() ) : ?>
      <?php while ( $blog_posts->have_posts() ) : $blog_posts->the_post(); ?>
        <div class="col-12 col-md-6 col-lg-4 my-2">
          <?php get_template_part( 'template-parts/partials/card-single-post', 'content' ); ?>
        </div>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
  <?php endif; ?>
  <?php
  wp_die();
}

function mytheme_add_woocommerce_support() {
  add_theme_support( 'woocommerce' );
  add_theme_support( 'wc-product-gallery-zoom' );
  add_theme_support( 'wc-product-gallery-lightbox' );
  add_theme_support( 'wc-product-gallery-slider' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

