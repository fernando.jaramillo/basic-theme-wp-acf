<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package BRANCH
 */
  get_header();
  $s = get_search_query();
?>

  <main id="taxonomy-page" class="site-main taxonomy-page">
    <section>

      <div class="container py-section">
        <div class="row">
          <div class="col-sm-12">
            <?php
              if ( have_posts() ) : ?>
                <div class="row pt-4 row-post d-flex justify-content-center">
                <h3 class="acg_primary_text ps-0">
                  Resultados de búsqueda para: <span class="acg_text_gray title-h2"><?php echo $s; ?></span>
                </h3>
                <hr class="separator-text"> <br>
                <p class="ps-0">
                  <?php esc_html_e( '¿Nada coincide con sus términos de búsqueda?. Vuelva a intentarlo con algunas palabras clave diferentes.', 'branch' ); ?>
                </p>
                <div class="form-div mb-5 ps-0">
                  <?php  get_search_form(); ?>
                </div>
                <section class="section-card-post-list">
                  <div class="row pt-4 d-flex justify-content-center"> 
                    <?php
                      while ( have_posts() ) : the_post();
                        $type = get_post_type( get_the_ID() );
                        echo '<div class="col-12 col-md-6 col-lg-3 my-2">';
                          get_template_part( 'template-parts/partials/card-single-post', 'content' );
                        echo '</div>';
                      endwhile; 
                    ?>
                    </div>
                  </div>
                </section>
                
            <?php 
              else: 
                get_template_part( 'template-parts/content', 'none' );
              endif;	
            ?>
          </div>
        </div>
      </div>

    </section>
  </main>

<?php get_footer(); ?>
