<?php
/**
 * The Home Page
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/ *
 * @package Branch
 */
  get_header();
?>

  <main id="home-page" class="site-main home-page">
    <section>

    <div class="container py-section">
        <div class="row">
          <div class="col-sm-12">
          <?php
            if ( have_posts() ) : ?>
              <section class="section-card-post-list">
                <div class="row pt-4 d-flex justify-content-center"> 
                  <?php
                    while ( have_posts() ) : the_post();
                      $type = get_post_type( get_the_ID() ); ?>
                      <div class="col-12 col-md-6 col-lg-3 my-2">
                        <?php get_template_part( 'template-parts/partials/card-single-post', 'content' ); ?>
                      </div>
                    <?php endwhile; 
                  ?>
                  </div>
                </div>
              </section>
          <?php 
            else:
              get_template_part( 'template-parts/content' );
            endif; 
          ?>
          </div>
        </div>
      </div>

    </section>
  </main>

<?php get_footer(); 