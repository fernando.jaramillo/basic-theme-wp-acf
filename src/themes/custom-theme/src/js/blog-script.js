var page = 2;
let posts_per_page = 3
jQuery(function($) {
    console.log("load jquery")
    $('body').on('click', '.loadmore', function() {
      console.log( this )
      $('.loadmore').html("Cargando ...")
        var data = {
            'action': 'load_posts_by_ajax',
            'page': page,
            'security': blog.security,
            'posts_per_page': posts_per_page,
        };
  
        $.post(blog.ajaxurl, data, function(response) {
            if($.trim(response) != '') {
              $('.loadmore').html("Cargar más")
              $('.blog-posts').append(response);
              page++;
            } else {
                $('.loadmore').html("No hay más por mostrar")
                $('.loadmore').addClass('disabled');
                $('.loadmore').attr("disabled", "disabled");
            }
        });
    });
});