window.onload = function () {
  console.log("THEME READY!");
  loadMenuLevel();
  nabvar_movil();
  gotoUpPage();
  showMenuHover();
  formSearchBanner();
  readMore();
}

// load menu Level
function loadMenuLevel() {
  // close all inner dropdowns when parent is closed
  document
    .querySelectorAll(".navbar .dropdown")
    .forEach(function (everydropdown) {
      everydropdown.addEventListener("hidden.bs.dropdown", function () {
        this.querySelectorAll(".submenu").forEach(function (everysubmenu) {
          everysubmenu.style.display = "none";
        });
      });
    });
  let submenus = [];
  document.querySelectorAll(".dropdown-menu a").forEach(function (element) {
    element.addEventListener("click", function (e) {
      let nextEl = this.nextElementSibling;
      let level_menu = this.getAttribute("data-level-menu");
      hiddenSubmenus();
      submenus[level_menu] = nextEl;
      if (nextEl && nextEl.classList.contains("submenu")) {
        e.preventDefault();
        e.stopPropagation();
        nextEl.style.display =
          nextEl.style.display == "block" ? "none" : "block";
        /* submenus.forEach(( element, index ) => {
            if( index < level_menu ) {
              element.style.display = "block"
            }
          }); */
      }
    });
  });
}

function hiddenSubmenus() {
  document.querySelectorAll(".dropdown-menu a").forEach(function (submenu) {
    if (submenu) {
      if (window.innerWidth > 992) {
        let nextEl = submenu.nextElementSibling;
        if (nextEl && nextEl.classList.contains("submenu")) {
          nextEl.style.display = "none";
        }
      }
    }
  });
}

function nabvar_movil() {
  let button_mobile = document.querySelector(".navbar-mobil-univa");
  let McButton = document.querySelector(".McButton");
  if (button_mobile) {
    button_mobile.addEventListener("click", function () {
      McButton.classList.toggle("active");
    });
  }
}

//  Go to init document
function gotoUpPage() {
  let btnUpPage = document.querySelector(".btn-univa-up");

  if (btnUpPage) {
    btnUpPage.addEventListener("click", function () {
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    });
  }
}

function formSearchBanner() {
  const btn_search = document.querySelector(".btn-search");
  const input_search = document.querySelector(".input-search");
  const wist_form_search = document.querySelector("#univa_form_banner_search");

  if (btn_search) {
    btn_search.addEventListener("click", function (event) {
      event.preventDefault();
      if (input_search) {
        if (
          input_search.value.trim().length >= 0 &&
          input_search.value.trim().length <= 2
        ) {
          var myAlert = document.getElementById("toastNotice"); //select id of toast
          var bsAlert = new bootstrap.Toast(myAlert); //inizialize it
          bsAlert.show(); //show it
        } else {
          wist_form_search.submit();
        }
      }
    });
  }
}

// BTN Search More
function readMore() {
  document.querySelectorAll(".btn-action-read-more").forEach(function (button) {
    if (button) {
      button.addEventListener("click", function (event) {
        event.stopPropagation();
        let content_read = event.target.closest(".read-more-partial");
        let dots = content_read.querySelector(".dots");
        let moreText = content_read.querySelector(".more");
        let btn_readBtn = content_read.querySelector(".readBtn");
        if (dots.style.display === "none") {
          dots.style.display = "inline";
          moreText.style.display = "none";
          btn_readBtn.innerHTML = 'Leer Más <i class="fas fas-down"></i>';
        } else {
          dots.style.display = "none";
          moreText.style.display = "inline";
          btn_readBtn.innerHTML = 'Leer Menos <i class="fas fas-up"></i>';
        }
      });
    }
  });
}