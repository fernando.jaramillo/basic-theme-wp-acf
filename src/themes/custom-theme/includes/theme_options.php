<?php

  // ADD OPTION PAGE GLOBAL CONF IN THEME
  if( function_exists('acf_add_options_page') ) { 

    // ADD OPTION PAGE
    acf_add_options_page(array(
      'page_title' 	  => 'THEME General Settings',
      'menu_title'	  => 'THEME Settings',
      'menu_slug' 	  => 'theme-general-settings',
      'capability'	  => 'edit_posts',
      'icon_url'      => 'dashicons-superhero',
      'update_button' => __('Update THEME theme', 'acf'),
      'position'      => 60,
      'autoload'      => true,
      'redirect'	    => false
    ));
    
    acf_add_options_sub_page(array(
      'page_title'    => 'THEME Header Settings',
      'menu_title'	  => 'Header',
      'parent_slug'	  => 'theme-general-settings',
    ));
    
    acf_add_options_sub_page(array(
      'page_title' 	  => 'THEME Footer Settings',
      'menu_title'	  => 'Footer',
      'parent_slug'	  => 'theme-general-settings',
    ));  

    acf_add_options_sub_page(array(
      'page_title' 	  => 'THEME 404 Page Settings',
      'menu_title'	  => '404 Page',
      'parent_slug'	  => 'theme-general-settings',
    ));  
    
  }
?>